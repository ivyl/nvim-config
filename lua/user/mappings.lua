local buffer = { buffer = true }
local expr   = { expr = true }

local function toggle_wo(name)
  return function()
    ---@diagnostic disable-next-line: assign-type-mismatch
    vim.wo[name] = not vim.wo[name]
  end
end

-- terminal: <C-w><C-direction> - escape terminal and switch window
vim.keymap.set("t", "<C-w><C-h>", "<C-\\><C-n><C-w>h")
vim.keymap.set("t", "<C-w><C-j>", "<C-\\><C-n><C-w>j")
vim.keymap.set("t", "<C-w><C-k>", "<C-\\><C-n><C-w>k")
vim.keymap.set("t", "<C-w><C-l>", "<C-\\><C-n><C-w>l")


-- <CR> - turn off hlsearch
vim.keymap.set({ "n", "v" }, "<CR>", vim.cmd.nohlsearch, { silent = true })
local nohl_group = vim.api.nvim_create_augroup("cmd-nofile-no-nohl", { clear = true })
vim.api.nvim_create_autocmd("CmdwinEnter", {
  group    = nohl_group,
  callback = function()
    vim.keymap.set("n", "<CR>", "<CR>", buffer)
  end
})
vim.api.nvim_create_autocmd("BufWinEnter", {
  group    = nohl_group,
  callback = function()
    if vim.bo.buftype == "nofile" or vim.bo.buftype == "quickfix" then
      vim.keymap.set("n", "<CR>", "<CR>", buffer)
    end
  end
})

-- <Leader>s - toggle spellchecking
vim.keymap.set("n", "<Leader>sp", toggle_wo("spell"), { silent = true, desc = "Toggle Spellchecking" } )

-- <Leader>n - toggle invisibles
vim.keymap.set("n", "<Leader>ll", toggle_wo("list"), { silent = true, desc = "Toggle Whitespace" })

-- <Leader>cc - toggle color column
local function toggle_color_column()
  if vim.wo.colorcolumn ~= "" then
    vim.wo.colorcolumn = ""
  else
    local cc = {}
    for i = 81,999 do table.insert(cc, i) end
    vim.wo.colorcolumn = table.concat(cc, ",")
  end
end
vim.keymap.set("n", "<leader>cc", toggle_color_column, { desc = "Toggle Color Column" })

-- command-mode: change behaior of <C-n> <C-p> to use the already typed text
vim.keymap.set("c", "<C-p>", "<Up>")
vim.keymap.set("c", "<C-n>", "<Down>")

-- command-mode: expand %% to curent file's dir
vim.keymap.set("c", "%%", function()
  if vim.fn.getcmdtype() == ":" then
    return vim.fn.expand("%:p:h") .. "/"
  else
    return "%%"
  end
end, expr)

-- g] - remap to use partial matches
vim.keymap.set("n", "]g", ":ts /<C-r><C-w><CR>")

-- <Leader>ab - address book
local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values
local actions = require "telescope.actions"
local action_state = require "telescope.actions.state"
vim.keymap.set('n', '<leader>ab', function()
  pickers.new({}, {
    prompt_title = "email address",
    finder = finders.new_oneshot_job({ "notmuch", "address", "*" }, {}),
    sorter = conf.generic_sorter(),
    attach_mappings = function(prompt_bufnr, _)
      actions.select_default:replace(function()
        actions.close(prompt_bufnr)
        local selection = action_state.get_selected_entry()
        vim.api.nvim_put({ selection[1] }, "", false, true)
      end)
      return true
    end
  }):find()
end,
{ desc = "Address Book" }
)

vim.keymap.set('n', '<leader>fo', require("telescope._extensions.neorg.find_norg_files"),
{ desc = "Find Neorg Files" }
)

vim.keymap.set('n', '<leader>fO', require("telescope._extensions.neorg.find_linkable"),
{ desc = "Grep Neorg Headings" }
)

vim.keymap.set('n', '<leader>ni', function()
  vim.cmd.Neorg("index")
end,
{ desc = "Goto Neorg Index" }
)

vim.keymap.set('n', '<leader>nr', function()
  vim.cmd.Neorg("return")
end,
{ desc = "Return From Neorg" }
)

vim.keymap.set('n', '<leader>np', function()
  vim.cmd.Neorg("workspace", "private")
end,
{ desc = "Goto Neorg Private Workspace" }
)

vim.keymap.set('n', '<leader>nw', function()
  vim.cmd.Neorg("workspace", "work")
end,
{ desc = "Goto Neorg Work Workspace" }
)

-- :Present
vim.api.nvim_create_user_command('Present', function ()
  package.loaded['user.present'] = nil
  require'user.present'
end, { force = true })

local lspconfig = require'lspconfig'

lspconfig.util.on_setup = lspconfig.util.add_hook_before(lspconfig.util.on_setup, function(config)
  if config.cmd[1]:find('clangd') then
    local workspace = config.workspace_folders[1].name
    local clangd_config_path = workspace .. '/.clangd'

    if vim.fn.filereadable(clangd_config_path) then
      local clangd_config = require'user.util'.load_yaml(clangd_config_path)
      if config.PathMappings then
        config.cmd = { 'clangd', "--path-mappings=" .. clangd_config.PathMappings }
      end
    end

  end
end)

vim.keymap.set('n', '<leader>d', vim.diagnostic.setloclist,
               { silent = true, desc = "LSP Diagnotics" })

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', { clear = true }),
  callback = function(args)
    local caps = vim.lsp.get_client_by_id(args.data.client_id).server_capabilities

    if caps then
      if caps.declarationProvider then
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration,
                       { silent = true, buffer=args.buf, desc = "LSP: Goto Declaration" })
      end

      if caps.definitionProvider then
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition,
                       { silent = true, buffer=args.buf, desc = "LSP: Goto Definition"})
      end

      if caps.hoverProvider then
        vim.keymap.set('n', '<A-k>', 'K',
                       { silent = true, buffer = args.buf })
      end

      if caps.implementationProvider then
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation,
                       { silent = true, buffer=args.buf, desc = "LSP: Goto Implementation" })
      end

      if caps.signatureHelpProvider then
        vim.keymap.set('n', '<leader>k', vim.lsp.buf.signature_help,
                       { silent = true, buffer=args.buf, desc = "LSP: Signature Help" })
      end
    end

      -- we don't have competing mappings for those below
      vim.keymap.set('n', '<leader>lwa', vim.lsp.buf.add_workspace_folder,
                     { silent = true, buffer=args.buf, desc = "LSP: Add Workspace Folder" })

      vim.keymap.set('n', '<leader>lwr', vim.lsp.buf.remove_workspace_folder,
                     { silent = true, buffer=args.buf, desc = "LSP: Remove Workspace Folder" })

      vim.keymap.set('n', '<leader>lwl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end,
                     { silent = true, buffer=args.buf, desc = "LSP: List Workspace Folders" })

      vim.keymap.set('n', '<leader>lrn',  vim.lsp.buf.rename,
                     { silent = true, buffer=args.buf, desc = "LSP: Rename" })

      vim.keymap.set('n', 'gr',          vim.lsp.buf.references,
                     { silent = true, buffer=args.buf, desc = "LSP: References" })

      vim.keymap.set('n', '<leader>lfmt', vim.lsp.buf.format,
                     { silent = true, buffer=args.buf, desc = "LSP: Format" })

      vim.keymap.set('n', '<leader>li', function()
        vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
      end,
      { silent = true, buffer=args.buf, desc = "LSP: Toggle Inline Hints" })
  end,
})

-- mini.bracketed doesn't have those
vim.keymap.set('n', ']a', vim.cmd.next, { silent = true })
vim.keymap.set('n', ']A', vim.cmd.last, { silent = true })
vim.keymap.set('n', '[a', vim.cmd.previous, { silent = true })
vim.keymap.set('n', '[A', vim.cmd.first, { silent = true })
