require("lazy").setup({
  -- better text editing
  "dhruvasagar/vim-table-mode",
  { "nmac427/guess-indent.nvim", opts = { filetype_exclude = { "gitcommit" } } };
  { "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = {
      indent = { char = "¦" },
      scope = { enabled = true, char = "|", show_start = false, show_end = false },
    }
  },

  { "ThePrimeagen/harpoon", branch = "harpoon2",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      local harpoon = require("harpoon")
      harpoon:setup()

      vim.keymap.set("n", "<leader>j", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

      vim.keymap.set("n", "<leader>z", function() harpoon:list():add() end)
      vim.keymap.set("n", "]z", function() harpoon:list():prev() end)
      vim.keymap.set("n", "[z", function() harpoon:list():next() end)
    end },


  -- straight from the pope
  "tpope/vim-repeat",
  "tpope/vim-fugitive",


  -- git
  { "lewis6991/gitsigns.nvim", opts = {
    on_attach = function(bufnr)
      vim.keymap.set("n", "]h", function() require("gitsigns").next_hunk() end, { buffer = bufnr })
      vim.keymap.set("n", "[h", function() require("gitsigns").prev_hunk() end, { buffer = bufnr })
      vim.keymap.set("n", "<Leader>gp", function() require("gitsigns").preview_hunk() end, { buffer = bufnr, desc = "Git Preview Hunk" })
      vim.keymap.set("n", "<Leader>gb", function() require("gitsigns").blame_line({ full = true, ignore_whitespace = true }) end, { buffer = bufnr, desc = "Git Blame Line" })
      vim.keymap.set("n", "<Leader>gs", function() require("gitsigns").stage_hunk() end, { buffer = bufnr, desc = "Git Stage Hunk" })
      vim.keymap.set("n", "<Leader>gr", function() require("gitsigns").reset_hunk() end, { buffer = bufnr, desc = "Git Reset Hunk" })
      vim.keymap.set("n", "<Leader>gu", function() require("gitsigns").undo_stage_hunk() end, { buffer = bufnr, desc = "Git Undo Stage Hunk" })
      vim.keymap.set("n", "<Leader>gd", function() require("gitsigns").toggle_deleted() end, { buffer = bufnr, desc = "Git Show Deleted Lines" })
    end }
  },

  { "sindrets/diffview.nvim", config = function()
      vim.keymap.set("n", "<Leader>gdo", ":DiffviewOpen<CR>", { desc = "Git Diff Open" })
      vim.keymap.set("n", "<Leader>gdc", ":DiffviewClose<CR>", { desc = "Git Diff Open" })
  end },


  -- my own plugins c:
  { "ivyl/bling.nvim", dev = true, opts = {} },
  { "ivyl/highlight-annotate.nvim", dev = true, opts = {
      mappings = { prev_annotation = "[v", next_annotation = "]v" }
    }
  },


  -- telescope
  { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
  { "nvim-telescope/telescope.nvim",
  dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-fzf-native.nvim",
      "ivyl/highlight-annotate.nvim",
      "nvim-telescope/telescope-symbols.nvim",
      "nvim-telescope/telescope-ui-select.nvim"
  },
  config = function()
    local telescope = require("telescope")
    local builtin = require("telescope.builtin")
    local themes = require("telescope.themes")
    vim.keymap.set("n", "<leader>fb", function() builtin.buffers(themes.get_dropdown{previewer=false}) end, { desc = "Live Grep" })
    vim.keymap.set("n", "<leader>fg", builtin.live_grep, { desc = "Live Grep" })
    vim.keymap.set("n", "<leader>fw", builtin.grep_string, { desc = "Find Current Word" })
    vim.keymap.set("n", "<leader>ft", builtin.tags, { desc = "Search Tags" })
    vim.keymap.set("n", "<leader>fd", builtin.diagnostics, { desc = "Search Diagnotics" })
    vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "Search Files" })
    vim.keymap.set("n", "<leader>flr", builtin.lsp_references, { desc = "Search LSP Refs" })
    vim.keymap.set("n", "<leader>flc", builtin.lsp_incoming_calls, { desc = "Search LSP Incoming Calls" })
    vim.keymap.set("n", "<leader>fld", builtin.lsp_definitions, { desc = "Search LSP Incoming Calls" })
    vim.keymap.set("n", "<leader>fls", builtin.lsp_document_symbols, { desc = "Search LSP Document Symbols" })
    vim.keymap.set("n", "<leader>fld", builtin.lsp_dynamic_workspace_symbols, { desc = "Search LSP Workspace Symbols" })
    vim.keymap.set("n", "<leader>fs", function() builtin.symbols{sources={"emoji", "math"}} end, { desc = "Emoji" })
    telescope.setup({ extensions =
      {
        fzf = { fuzzy = true, override_generic_sorter = true, override_file_sorter = true, case_mode = "smart_case" },
        ["ui-select"] = { require("telescope.themes").get_dropdown()}
      }
    })
    telescope.load_extension("fzf")
    telescope.load_extension("ui-select")
    telescope.load_extension("highlight-annotate")
    vim.keymap.set("n", "<leader>fa", telescope.extensions["highlight-annotate"].annotations, { desc = "Search Annotations" } )
  end },


  -- nvim-cmp
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-nvim-lsp-signature-help",
  "hrsh7th/cmp-nvim-lua",
  "hrsh7th/cmp-buffer",
  { "L3MON4D3/LuaSnip", build = "make install_jsregexp" },
  "saadparwaiz1/cmp_luasnip",
  { "hrsh7th/nvim-cmp", config = function()
    local cmp = require("cmp")
    cmp.setup({
      snippet = { expand = function(args) require('luasnip').lsp_expand(args.body) end },
      experimental = { ghost_text = true },
      mapping = cmp.mapping.preset.insert({
        ["<C-u>"] = cmp.mapping.scroll_docs(-4),
        ["<C-d>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
      }),
      sources = cmp.config.sources({
        { name = "neorg" },
        { name = "nvim_lsp" },
        { name = "nvim_lsp_signature_help" },
        { name = "nvim-lua" },
        { name = "buffer" },
        { name = "luasnip" },
        { name = "lazydev", group_index = 0 },
      }),
      view = {
        entries = { name = "custom", selection_order = "near_cursor" }
      },
      formatting = {
        fields = { "abbr", "kind", "menu" },
        format = function(entry, item)
          item.menu = "[" .. entry.source.name .. "]"
          return item
        end
      }
    })
  end },


  -- LSP
  { "neovim/nvim-lspconfig", dependencies = {"hrsh7th/nvim-cmp"}, config = function()
    local capabilities = require("cmp_nvim_lsp").default_capabilities()
    local lspconfig = require("lspconfig")
    local servers = { "clangd", "pylsp", "rust_analyzer", "lua_ls", "solargraph", "marksman", "ruff_lsp", "pyright" }
    for _, lsp in ipairs(servers) do
      lspconfig[lsp].setup({
        capabilities = capabilities,
        settings = {
          Lua = {
            runtime = { version = "LuaJIT" },
            diagnostics = { globals = { 'vim' } },
            workspace = { library = vim.api.nvim_get_runtime_file("", true) }
          },
          ["rust-analyzer"] = {
            checkOnSave  = { command =  "clippy" }
          }
        }
      })
    end
  end },

  { "https://git.sr.ht/~whynothugo/lsp_lines.nvim", config = function()
    require("lsp_lines").setup()
    vim.diagnostic.config({ virtual_text = false })
  end },

  { "aznhe21/actions-preview.nvim", config = function()
    vim.keymap.set({ "v", "n" }, "<leader>la", require("actions-preview").code_actions, { desc = "LSP: Code Action" })
  end },


  -- neorg
  {
    "vhyrro/luarocks.nvim",
    priority = 1000,
    config = true,
  },
  {
    "nvim-neorg/neorg",
    tag = "v8.4.1",
    opts = {
      load = {
        ["core.defaults"] = {},
        ["core.summary"] = {},
        ["core.integrations.telescope"] = {},
        ["core.completion"] = { config = { engine = "nvim-cmp", name = "[Norg]" } },
        ["core.integrations.nvim-cmp"] = {},
        ["core.concealer"] = {
          config = {
            icon_preset = "diamond",
            icons = {
              todo = {
                  done = { icon = "✓" },
                  pending = { icon = "…" },
                  uncertain = { icon = "‽" },
                  cancelled = { icon = "■" },
                  recurring = { icon = "⌽" },
              },
            },
          }
        },
        ["core.keybinds"] = {
            config = {
                default_keybinds = true,
                neorg_leader = "<Leader><Leader>",
                hook = function(keybinds)
                    keybinds.remap_key("norg", "n", "<CR>", "<C-]>")
                    keybinds.map("norg", "n", keybinds.leader .. "il", function() require("telescope._extensions.neorg.insert_link")() end, { desc = "Insert Link" })
                    keybinds.map("norg", "n", keybinds.leader .. "if", function() require("telescope._extensions.neorg.insert_file_link")() end, { desc = "Insert File Link" })
                end,
            },
        },
        ["core.dirman"] = {
            config = {
                workspaces = {
                    work = "~/sync/norg-work/",
                    private = "~/sync/norg-priv/",
                },
                default_workspace = "work"
            },
        },
        ["core.export"] = {},
        ["core.export.markdown"] = {},
        ["core.tempus"] = {},
        ["core.ui.calendar"] = {},
      },
    },
    dependencies = { "nvim-lua/plenary.nvim",  "nvim-treesitter/nvim-treesitter", "nvim-treesitter/nvim-treesitter-textobjects", "hrsh7th/nvim-cmp", "nvim-neorg/neorg-telescope", "luarocks.nvim" },
},


  -- treesitter
  "andymass/vim-matchup",
  "nvim-treesitter/nvim-treesitter-textobjects",
  { "nvim-treesitter/nvim-treesitter",
  build = function() require"nvim-treesitter.install".update({ with_sync = true }) end,
  config = function()
    ---@diagnostic disable-next-line: missing-fields
    require"nvim-treesitter.configs".setup({
      ensure_installed = "all",
      sync_install = false,
      auto_install = true,
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = {"org"},
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<c-space>",
          node_incremental = "<c-space>",
          node_decremental = "<c-backspace>",
        }
      },
      textobjects = {
        select = {
          enable = true,
          lookahead = true,
          keymaps = {
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
          },
        },
        move = {
          enable = true,
          set_jumps = true,
          goto_next_start = {
            ["]m"] = "@function.outer",
            ["]]"] = "@class.outer",
          },
          goto_next_end = {
            ["]M"] = "@function.outer",
            ["]["] = "@class.outer",
          },
          goto_previous_start = {
            ["[m"] = "@function.outer",
            ["[["] = "@class.outer",
          },
          goto_previous_end = {
            ["[M"] = "@function.outer",
            ["[]"] = "@class.outer",
          },
        },
        matchup = {
          enable = true,
        },
        swap = {
          enable = true,
          swap_next = { ["<leader>p"] = "@parameter.inner" },
          swap_previous = { ["<leader>P"] = "@parameter.inner" },
        },
      },
    })
  end },

  "nvim-treesitter/playground",

  -- mini.nvim
  { "echasnovski/mini.nvim", config = function()
    vim.api.nvim_create_user_command("BD", function() require("mini.bufremove").delete(0, false) end, { force = true })

    require("mini.align").setup()
    require("mini.bracketed").setup()
    require("mini.ai").setup()

    -- mini.clue
    local miniclue = require("mini.clue")
    miniclue.setup({
      triggers = {
        { mode = "n", keys = "<Leader>" },
        { mode = "x", keys = "<Leader>" },
        { mode = "i", keys = "<C-x>" },
        { mode = "n", keys = "g" },
        { mode = "x", keys = "g" },
        { mode = "n", keys = "'" },
        { mode = "n", keys = "`" },
        { mode = "x", keys = "'" },
        { mode = "x", keys = "`" },
        { mode = "n", keys = '"' },
        { mode = "x", keys = '"' },
        { mode = "i", keys = "<C-r>" },
        { mode = "c", keys = "<C-r>" },
        { mode = "n", keys = "<C-w>" },
        { mode = "n", keys = "z" },
        { mode = "x", keys = "z" },
      },
      clues = {
        miniclue.gen_clues.builtin_completion(),
        miniclue.gen_clues.g(),
        miniclue.gen_clues.marks(),
        miniclue.gen_clues.registers(),
        miniclue.gen_clues.windows(),
        miniclue.gen_clues.z(),
      },
      window = {
        config = { relative = "cursor", anchor = "NW", row = "auto", col = "auto", width = "auto" },
      }
    })

    require("mini.surround").setup({
      custom_surroundings = {
        ["("] = { output = { left = "( ", right = " )" } },
        ["["] = { output = { left = "[ ", right = " ]" } },
        ["{"] = { output = { left = "{ ", right = " }" } },
        ["<"] = { output = { left = "< ", right = " >" } },
      },
      mappings = {
        add = "ys",
        delete = "ds",
        find = "",
        find_left = "",
        highlight = "",
        replace = "cs",
        update_n_lines = "",
      },
      search_method = "cover_or_next",
    })

    -- mini.statusline
    local ministatusline = require("mini.statusline")
    local active_content = function()
      local mode, mode_hl = ministatusline.section_mode({ trunc_width = 50 })
      local filename      = ministatusline.section_filename({ trunc_width = 999 }) -- always truncate
      local git = {}
      local diagnostics = {}

      if vim.b["gitsigns_status_dict"] then
        git.head = "  " .. vim.b["gitsigns_status_dict"].head
        for _, op in ipairs({ { "added", "+" }, { "removed", "-" }, { "changed", "~" } }) do
          local value = vim.b["gitsigns_status_dict"][op[1]]
          if value and value > 0 then
            git[op[1]] = op[2] .. tostring(value)
          end
        end
      end

      local diag_severities = {
        {"E", vim.diagnostic.severity.ERROR, "Error"},
        {"W", vim.diagnostic.severity.WARN, "Warn"},
        {"I", vim.diagnostic.severity.INFO, "Info"},
        {"H", vim.diagnostic.severity.HINT, "Hint"},
      }

      for _, diag in ipairs(diag_severities) do
        local count = #vim.diagnostic.get(0, {severity = diag[2]})
        if count > 0 then
          if diagnostics["icon_hl"] == nil then
            diagnostics["icon_hl"] = "MiniStatuslineDiagnostics" .. diag[3]
          end
          diagnostics.icon = ""
          diagnostics[diag[1]] = diag[1] .. ":" .. tostring(count)
        end
      end

      if diagnostics["icon_hl"] == nil then
        diagnostics["icon_hl"] = ""
      end

      local indent_style = ""
      if vim.o.expandtab then
        indent_style = "␣ " .. tostring(vim.o.softtabstop)
      else
        indent_style = "⭾ " .. tostring(vim.o.tabstop)
      end

      return ministatusline.combine_groups({
        { hl = mode_hl,                          strings = { mode } },
        { hl = "MiniStatuslineIndentStyle",      strings = { indent_style } },
        { hl = "MiniStatuslineGitHead",          strings = { git.head } },
        { hl = "MiniStatuslineGitAdded",         strings = { git.added } },
        { hl = "MiniStatuslineGitRemoved",       strings = { git.removed } },
        { hl = "MiniStatuslineGitChanged",       strings = { git.changed } },
        { hl = diagnostics["icon_hl"],           strings = { diagnostics.icon } },
        { hl = "MiniStatuslineDiagnosticsError", strings = { diagnostics.E } },
        { hl = "MiniStatuslineDiagnosticsWarn",  strings = { diagnostics.W } },
        { hl = "MiniStatuslineDiagnosticsInfo",  strings = { diagnostics.I } },
        { hl = "MiniStatuslineDiagnosticsHint",  strings = { diagnostics.H } },
        "%<", "%=",
        { hl = "MiniStatuslineFilename",         strings = { filename } },
      })
    end
    ministatusline.setup({
      content = {
        active = active_content,
        inactive = nil,
      },
      use_icons = false,
    })

    -- mini.move
    require("mini.move").setup({
       mappings = {
          left = "<C-h>",
          right = "<C-l>",
          down = "<C-j>",
          up = "<C-k>",
          line_left = "<C-h>",
          line_right = "<C-l>",
          line_down = "<C-j>",
          line_up = "<C-k>",
      }
    })

    -- minitrailspace
    local minitrailspace = require("mini.trailspace")
    minitrailspace.setup()
    -- there's a quirk where lazy.nvim's float has trailspace enabled
    -- let's workaround this issue
    vim.api.nvim_create_autocmd("WinEnter", {
      group = vim.api.nvim_create_augroup("mini.trailspace.user", { clear = true }),
      callback = function()
        if vim.api.nvim_win_get_config(0).relative ~= '' then
          minitrailspace.unhighlight()
        end
      end
    })
  end},

  { "m00qek/baleia.nvim", tag = "v1.3.0", config = function()
    local baleia = require("baleia").setup()
    vim.api.nvim_create_user_command("ANSI", function() baleia.once(vim.fn.bufnr("%")) end, { force = true })
  end },

  { "smoka7/hop.nvim", config = function()
    local hop = require("hop")
    hop.setup()
    vim.keymap.set("", "<leader>h", function() hop.hint_anywhere() end, { desc = "Hop Anywhere" })
    vim.keymap.set("", "<leader>w", function() hop.hint_words() end, { desc = "Hop Words" })
  end },


  -- nvim config development
  "milisims/nvim-luaref",
  "nanotee/luv-vimdocs",
  { "folke/lazydev.nvim",
    ft = "lua", -- only load on lua files
    opts = {
      library = {
        { path = "luvit-meta/library", words = { "vim%.uv" } },
      },
    },
  },
  { "Bilal2453/luvit-meta", lazy = true }, -- optional `vim.uv` typings


  -- :SudaWrite :SudaRead
  "lambdalisue/suda.vim",


  -- presentations
  { "catppuccin/nvim", name = "catppuccin" },
  { "folke/zen-mode.nvim",
  opts = {
    window = {
      width = 80,
      options = {
        signcolumn = "no",
        number = false,
        relativenumber = false,
        cursorline = false,
        cursorcolumn = false,
      },
    }
  }
  },

  -- file management
  {
    "stevearc/oil.nvim", config = function()
      require("oil").setup({
          columns = {
            "permissions",
            "size",
            "mtime",
          },
          view_options = { show_hidden = true },
      })
      vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })
    end
  },
}, {
  dev = { path = "~/src" },
  ui = {
    border = "rounded",
    icons = {
      cmd = " ",
      event = "📅",
      import = " ",
      lazy = "💤 ",
      plugin = "🔌",
      runtime = "💻",
      require = "🌙",
      start = " ",
      source = "  ",
      task = "✔ ",
    },
  }
})
