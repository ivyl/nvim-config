local licenses = require('user.licenses')
local mini_comment = require('mini.comment')

local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local i = ls.insert_node
local d = ls.dynamic_node
local t = ls.text_node
local f = ls.function_node

local extras = require("luasnip.extras")
local partial = extras.partial

vim.keymap.set({"i", "s"}, "<Tab>", function() ls.jump(1) end, {silent = true})
vim.keymap.set({"i", "s"}, "<S-Tab>", function() ls.jump(-1) end, {silent = true})

vim.keymap.set({"i", "s"}, "<C-E>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end, {silent = true})

local function get_git_user()
  local user_name  = vim.fn.trim(vim.fn.system("git config user.name") or "unable to get user name")
  local user_email = vim.fn.trim(vim.fn.system("git config user.email") or "unable to get user email")
  return user_name .. " <" .. user_email .. ">"
end

-- [[ EMAIL / WRITEUPS SNIPPETS ]]

ls.add_snippets("all", {
  s(
    { trig = "sob", name = "sign off git commit" },
    { t("Signed-off-by: "), f(get_git_user) }
    ),
    s(
      { trig = "rb", name = "review-by git commit" },
      { t("Reviewed-by: "), f(get_git_user) }
    ),
    s(
      { trig = "ack", name = "ack git commit" },
      { t("Acked-by: "), f(get_git_user) }
    ),
    s(
      { trig = "sig", name = "insert mail signature" },
      { t({ "-- ", "Cheers,", "Arek" }) }
    ),
})

-- [[ LICENSE HELPERS ]]

local function get_name()
  return vim.fn.trim(vim.fn.system("git config user.name") or "unable to get user name") or ""
end

local function get_year()
  return os.date("%Y")
end

local function license_sub(text)
  local ret = string.gsub(text, "{year}", os.date("%Y"))
  ret = string.gsub(ret, "{copyright_holder}", get_name())
  return ret
end

local function get_comment_string()
  return mini_comment.get_commentstring({ vim.api.nvim_win_get_cursor(0)[1], 1 })
end

local function wrap_into_comment_c(text, include_opening)
  local result = {}

  if (include_opening) then
    table.insert(result, "/*")
  end
  for line in string.gmatch(text .. "\n", "(.-)\n") do
    local commented_line = (" * %s"):gsub("%%s", line, 1)
    table.insert(result, vim.fn.trim(commented_line, " ", 2))
  end
  table.insert(result, " */")

  return result
end

local function wrap_into_comment_commentstring(text, _)
  local result = {}
  local comment_pattern = get_comment_string()
  for line in string.gmatch(text .. "\n", "(.-)\n") do
    local commented_line = comment_pattern:gsub("%%s", " " .. line .. " ", 1)
    table.insert(result, vim.fn.trim(commented_line))
  end
  return result
end

local function wrap_into_comment(text, include_opening)
  if vim.bo.filetype == "c" then
    return wrap_into_comment_c(text, include_opening)
  end
  return wrap_into_comment_commentstring(text, include_opening)
end

local function comment_open()
  return {"/*", " * "}
end

local function comment_eol()
  return ""
end

local function create_license_snippet(license_text)
  return {
        f(comment_open),
        t("Copyright "), i(1, get_year()), t(" "), i(2, get_name()), i(0),
        f(comment_eol), t({"", " *", ""}),
        f(function() return wrap_into_comment(license_text, false) end)
  }
end

-- [[ LICENSE SNIPPEETS ]]

ls.add_snippets("all", {
    s(
      { trig = "MIT", name = "MIT License Comment" },
      create_license_snippet(licenses.MIT_TEXT)
    ),
    s(
      { trig = "LGPL", name = "LGPL 2.1 License Comment" },
      create_license_snippet(licenses.LGPL_TEXT)
    ),
})
