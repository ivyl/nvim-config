local base03    = { "#002b36", 8  }
local base02    = { "#073642", 0  }
local base01    = { "#586e75", 10 }
local base00    = { "#657b83", 11 }
local base0     = { "#839496", 12 }
local base1     = { "#93a1a1", 14 }
local base2     = { "#eee8d5", 7  }
local base3     = { "#fdf6e3", 15 }
local yellow    = { "#b58900", 3  }
local orange    = { "#cb4b16", 9  }
local red       = { "#dc322f", 1  }
local magenta   = { "#d33682", 5  }
local violet    = { "#6c71c4", 13 }
local blue      = { "#268bd2", 4  }
local cyan      = { "#2aa198", 6  }
local green     = { "#859900", 2  }

local none = { "NONE",    "NONE" }
local back = { base03[1], "NONE" }

local baseX0 = base00;
local baseX1 = base01;
local baseX2 = base02;
local baseX3 = base03;
local baseY0 = base0;
local baseY1 = base1;
local baseY2 = base2;
local baseY3 = base3;

if vim.o.background == "light" then
  back = base3
  baseX0 = base0;
  baseX1 = base1;
  baseX2 = base2;
  baseX3 = base3;
  baseY0 = base00;
  baseY1 = base01;
  baseY2 = base02;
  baseY3 = base03;
end

local bold      = "bold"
local italic    = "italic"
local reverse   = "reverse"
local underline = "underline"
local undercurl = "undercurl"
local standout  = "standout"

local c = {
  -- core nvim
  Neutral    = { fg = baseY0, bg = back },
  Normal     = { fg = baseY0, bg = back },
  Comment    = { fg = baseX1, { italic } },
  Constant   = { fg = cyan },
  String   = { fg = cyan },
  Identifier = { fg = blue },
  Function = { fg = blue },
  ["@variable"] = { fg = blue },
  Statement  = { fg = green },
  Operator   = { fg = green },
  PreProc    = { fg = orange },
  Type       = { fg = yellow },
  Special    = { fg = red },
  Delimiter  = { fg = red },
  Underline  = { fg = violet },
  Ignore     = { fg = none },
  Error      = { fg = red, { bold } },
  Todo       = { fg = magenta, { bold } },
  SpecialKey = { fg = baseX0, bg = baseX2, { bold } },
  NonText    = { fg = baseX0, { bold } },

  StatusLine   = { fg = baseY1,  bg = baseX2, { reverse } },
  StatusLineNC = { fg = base00, bg = baseX2, { reverse } },
  Visual       = { fg = base01, bg = baseX3, { reverse } },

  Directory = { fg = blue },
  ErrorMsg  = { fg = red, { reverse } },

  Search    = { fg = yellow,  { standout } },
  IncSearch = { fg = orange,  { standout } },

  MoreMsg = { fg = blue },
  ModeMsg = { fg = blue },

  LineNr = { fg = base01, bg = baseX2 },
  Question = { fg = cyan, { bold } },

  VertSplit = { fg = base00, bg = baseX0 },

  Title = { fg = orange, { bold } },
  VisualNOS = { fg = none, bg = baseX2, { reverse, standout } },

  WarningMsg     = { fg = red, { bold } },
  WildMenu       = { fg = baseY2, bg = base01, { reverse } },
  Folded         = { fg = baseY0, bg = base02, { bold, underline } },
  FoldColumn     = { fg = baseY0, bg = base02 },

  DiffAdd        = { fg = green,  bg = baseX2, { bold } },
  DiffChange     = { fg = yellow, bg = baseX2, { bold } },
  DiffDelete     = { fg = red,    bg = baseX2, { bold } },
  DiffText       = { fg = blue,   bg = baseX2, { bold } },

  SignColumn     = { fg = baseY0 },

  Conceal        = { fg = blue, bg = none },

  SpellBad       = { fg = none, { undercurl } },
  SpellCap       = { fg = none, { undercurl } },
  SpellRare      = { fg = none, { undercurl } },
  SpellLocal     = { fg = none, { undercurl } },

  Pmenu          = { fg = baseY1,  bg = base02 },
  PmenuSel       = { fg = baseY2,  bg = magenta },
  PmenuSbar      = { fg = baseY0,  bg = base03 },
  PmenuThumb     = { fg = base03, bg = magenta },
  TabLine        = { fg = baseY0,  bg = baseX2,  { underline } },
  TabLineFill    = { fg = baseY0,  bg = baseX2,  { underline } },
  TabLineSel     = { fg = baseY2,  bg = magenta, { underline } },
  CursorColumn   = { fg = none,   bg = baseX2 },
  CursorLine     = { fg = none,   bg = baseX2 },
  ColorColumn    = { fg = none,   bg = baseX2 },
  Cursor         = { fg = base03, bg = baseY0 },
  lCursors       = "Cursor",
  MatchParen     = { fg = red, bg = base01, { bold }},

  -- tree-sitter
  TSKeyword        = { fg = orange },
  TSPunctDelimiter = { fg = baseY1 },
  TSConstructor    = { fg = magenta },
  TSBoolean        = { fg = blue },
  TSFloat          = "TSBoolean",
  TSNumber         = "TSBoolean",
  TSField          = "Neutral",
  TSProperty       = "TSField",
  TSParameter      = "Neutral",
  TSPunctBracket   = "Neutral",

  -- telescope
  TelescopeSelection      = { fg = baseY3, bg = magenta, {bold} },
  TelescopeMultiSelection = { fg = baseY3, bg = violet },
  TelescopeBorder         = { fg = magenta },
  TelescopeSelectionCaret = { fg = magenta },
  TelescopePromptPrefix   = { fg = magenta },
  TelescopeMatching       = { fg = baseY1 },
  TelescopeTitle          = { fg = baseY2, {bold} },

  -- nvim-cmp
  CmpItemAbbr           = { fg = none },
  CmpItemAbbrDeprecated = { fg = magenta },
  CmpItemKind           = { fg = magenta },
  CmpItemAbbrMatch      = { fg = cyan },
  CmpItemAbbrMatchFuzzy = "CmpItemAbbrMatch",
  CmpItemMenu           = { fg = blue },

  -- LSP
  DiagnosticError = { fg = red,    { bold } },
  DiagnosticWarn  = { fg = yellow, { bold } },
  DiagnosticInfo  = { fg = blue,   { bold } },
  DiagnosticHint  = { fg = violet, { bold } },

  -- mini.statusline
  MiniStatuslineModeNormal  = { fg = baseY2, bg = baseX1 },
  MiniStatuslineModeInsert  = { fg = baseY2, bg = blue },
  MiniStatuslineModeVisual  = { fg = baseY2, bg = magenta },
  MiniStatuslineModeReplace = { fg = baseY2, bg = red },
  MiniStatuslineModeCommand = { fg = baseY2, bg = yellow },
  MiniStatuslineModeOther   = { fg = baseY2, bg = violet },

  MiniStatuslineGitHead      = { fg = blue,   bg = baseX3 },
  MiniStatuslineGitAdded     = { fg = green,  bg = baseX3 },
  MiniStatuslineGitRemoved   = { fg = red,    bg = baseX3 },
  MiniStatuslineGitChanged   = { fg = yellow, bg = baseX3 },

  MiniStatuslineIndentStyle  = { bg = baseX2 },

  MiniStatuslineDiagnosticsError = { fg = red, bg = baseX2 },
  MiniStatuslineDiagnosticsWarn  = { fg = yellow, bg = baseX2 },
  MiniStatuslineDiagnosticsInfo  = { fg = blue, bg = baseX2 },
  MiniStatuslineDiagnosticsHint  = { fg = violet, bg = baseX2 },

  MiniStatuslineFilename     = { fg = baseY1, bg = baseX2 },
  MiniStatuslineLocation     = { fg = baseY1, bg = baseX2 },
  MiniStatuslineInactive     = { fg = baseY1, bg = baseX2 },

  -- mini.trailspace
  MiniTrailspace = "ErrorMsg",

  -- mini.clue
  MiniClueBorder = { fg = magenta, bg = back },
  MiniClueTitle = { fg = baseY1, bg = back, { bold } },
  MiniClueDescGroup = { fg = magenta, bg = baseX3 },
  MiniClueDescSingle = { fg = baseY2, bg = baseX3 },
  MiniClueNextKey = { fg = baseY1, bg = baseX3, { bold } },
  MiniClueSeparator = { fg = baseX1, bg = baseX3 },

  IblIndent   = { fg = baseX0 },
  IblScope    = { fg = baseX0 },

  GitSignsAdd = { fg = green },
  GitSignsChange = { fg = yellow },
  GitSignsDelete = { fg = red },

  mailSignature = { fg = red },

  mailQuoted1 = { fg = blue },
  mailQuoted2 = { fg = magenta },
  mailQuoted3 = { fg = yellow },
  mailQuoted4 = { fg = base01 },

  ["@text.strong"] = {  { bold } },
  ["@text.emphasis"] = {  { italic } },
  ["@neorg.markup.verbatim"] = { fg = baseX0 },
  ["@neorg.todo_items.on_hold.norg"] = { fg = baseX0 },
  ["@neorg.todo_items.cancelled.norg"] = { fg = red },
  ["@neorg.todo_items.undone.norg"] = { fg = magenta },
  ["@neorg.todo_items.urgent.norg"] = { fg = yellow },
  ["@neorg.todo_items.pending.norg"] = { fg = orange },
  ["@neorg.todo_items.recurring.norg"] = { fg = violet },
  ["@neorg.links.description.norg"] = { fg = blue, { underline } },
  ["@neorg.links.location.url.norg"] = { fg = blue, { underline } },
  ["@neorg.tags.ranged_verbatim.code_block"] = { bg = baseX2 },
  ["@attribute"] = { fg = magenta },
  ["@markup.quote"] = { fg = baseX0 },
  ["@markup.list"] = { fg = orange },
  ["@markup.strong"] = { { bold } },
  ["@markup.heading"] = { fg = cyan },
  ["@markup.heading.1"] = { fg = magenta },
  ["@markup.heading.2"] = { fg = green },
  ["@markup.link"] = "@text.uri",
  ["@markup.link.label"] = "@type",
  ["@markup.link.url.markdown_inline"] = { fg = blue, { underline } },
  ["@markup.raw"] = "@text.literal",
  ["@markup.italic"] = { { italic } },

  ["LazyButtonActive"] = { fg = baseY3, bg = magenta },
  ["LazySpecial"] = { fg = blue, { bold } },
  ["LazyH1"] = { fg = baseY3, bg = magenta, { bold } },

  ["FloatBorder"] = { fg = baseX2 },
  ["NormalFloat"] = { bg = baseX3 },
}

vim.cmd.highlight 'clear'

vim.g.colors_name = 'solarized'

for group, raw_settings in pairs(c) do
  local settings = {}

  if type(raw_settings) == "string" then
    settings.link = raw_settings
    goto end_hl
  end

  if not raw_settings.fg then raw_settings.fg = none end
  settings.fg = raw_settings.fg[1]
  settings.ctermfg = raw_settings.fg[2]

  if not raw_settings.bg then raw_settings.bg = none end
  settings.bg = raw_settings.bg[1]
  settings.ctermbg = raw_settings.bg[2]

  for _, property in ipairs(raw_settings[1] or {}) do
    settings[property] = true
  end

  ::end_hl::

  vim.api.nvim_set_hl(0, group, settings)
end
